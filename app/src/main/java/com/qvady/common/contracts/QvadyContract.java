package com.qvady.common.contracts;

public class QvadyContract {


    public static final String PROVIDER_AUTHORITY = "content://com.qvady.dataprovider.provider";

    // Method names to be used by apps using this cp
    public static final String READ_PREF_METHODNAME = "getPref";
    public static final String WRITE_PREF_METHODNAME = "setPref";

    public static final String INTENT_FILTER_WIFISETTINGS = "qvady.intent.action.WIFISETTINGS";


    // Keys of sharedprefs
    // Todo Restrict write access
    public static final String KEY_USERNAME = "username";
    public static final String KEY_USERID = "userid";
    public static final String KEY_USERTOKEN = "usertoken";
    public static final String DEFAULT_VAL = "default_val";


    //Constants
    public static final Integer DOWNLOAD_DEFAULT_ID = -1;
    public static final Integer DEFAULT_VAL_INT = -1;
}
