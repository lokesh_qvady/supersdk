package com.qvady.sdk.contracts

import android.app.DownloadManager
import com.qvady.common.models.AppInfo

interface UpdateContract {

    fun getLatestApps() : List<AppInfo>

    fun downloadApp(appInfo: AppInfo): Long

    fun downloadApp(appInfo: AppInfo,needSync: Boolean): Long

    fun getDownloadProgress(id: Long): Int

    fun getDownloadStatus(id: Long): Int

    fun getInstalledApp(packageName: String): AppInfo

    fun getInstalledApps() : List<AppInfo>?

    fun sync() : Boolean

    fun syncStatus(): Int

    fun getLatestAppsByUser(): List<AppInfo>?

    fun isUpdateAvailable(packageName: String): AppInfo?

    fun downloadApp(packageName: String): Long


}