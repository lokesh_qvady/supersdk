package com.qvady.sdk.contracts

import com.qvady.common.models.AppInfo

/* Contract for user details */
 interface UserContract {
    fun getId(): String
    fun getName(): String
    fun getApps(): List<AppInfo>
}