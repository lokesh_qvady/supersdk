package com.qvady.common.models

import com.qvady.common.contracts.QvadyContract.DOWNLOAD_DEFAULT_ID

class AppInfo {

    constructor()

    constructor(name: String,version_code: Int,enable: Boolean){
        this.package_name = name
        this.version_code = version_code
        this.enable = enable
    }

    public var package_name: String = ""

    public var version_code = 0

    public var version: String? = null

    public var app_name: String? = ""

    public var url: String? = null

    public var enable = false

    public var download_id = DOWNLOAD_DEFAULT_ID

}