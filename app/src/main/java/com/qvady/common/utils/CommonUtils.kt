package com.qvady.common.utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.provider.Settings
import android.support.annotation.NonNull
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.qvady.common.contracts.QvadyContract

class CommonUtils {

    companion object {

        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun isValidString(target: CharSequence?): Boolean {
            return target!=null && !TextUtils.isEmpty(target)
        }

        fun hideKeyboard(@NonNull view: View){
            val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun isConnectedToWifi(context: Context): Boolean{
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork?.isConnectedOrConnecting == true
        }

        // Opens Aosp Settings App
        fun openWifiSettings(context: Context) {
            val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
            context.startActivity(intent)
        }


        // Opens Qvady  Settings App
        fun connectToWifi(context: Context){
            val intent =  Intent(QvadyContract.INTENT_FILTER_WIFISETTINGS)
            context.startActivity(intent)
        }
    }

}