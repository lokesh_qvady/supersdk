package com.qvady.common.utils

import android.util.Log
import com.qvady.qvadycommon.BuildConfig
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException

class LogUtil {

    companion object {

        const val ERROR_TAG = "error"

        // Crashlytics Constants
        const val DOWNLOAD_ERROR = "download_error"
        const val STORAGE_ERROR = "storage_error"

        const val INSTALL_INITIATED = "install_initiated"
        const val DOWNLOAD_COMPLETED = "download_completed"
        const val APP_INSTALLED = "app_installed"

        const val PRIORITY_HIGH = 0

        fun log(priority: Int,tag: String,msg: String){
            if(BuildConfig.DEBUG){
                Log.d(tag,msg)
            }else{
//                Crashlytics.log(priority,tag,msg)
            }
        }

        fun log(msg: String){
            Log.d("lokesh",msg)
            appendLog(msg)
        }

        fun logException(tag: String,e: Exception){
            if(BuildConfig.DEBUG){
                Log.d(tag,e.toString())
            }else{
//                Crashlytics.logException(e)
            }
        }

        fun logException(e:Exception){
            logException(ERROR_TAG,e)
        }

        fun appendLog(text: String) {

            val root = android.os.Environment.getExternalStorageDirectory()
            val dir = File(root.absolutePath + "/download")
            dir.mkdirs()
            val file = File(dir, "log.txt")

            try {
                //BufferedWriter for performance, true to set append to file flag
                val buf = BufferedWriter(FileWriter(file, true))
                buf.append(text)
                buf.newLine()
                buf.close()
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        }

    }

}